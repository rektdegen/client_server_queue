"""threading example"""
from time import sleep
from threading import Thread
import uuid


class AddingMachine():
    """a queue machine
    enqueue
    dequeue
    start and stop worker
    return height"""
    def __init__(self, logger):
        self.the_queue = []
        self.active = 'off'
        self.logger = logger
        self.working_thread = Thread(target=self.worker)

    def worker(self):
        """this worker removes objects from queue
        wait 1 second for demonstration purposes"""
        while self.active == 'on':
            if self.the_queue:
                self.logger.debug('finished: %s' % self.the_queue[0][1])
                del self.the_queue[0]
            sleep(1)
        # exiting worker
        self.active = 'off'
        self.logger.info('stopping worker')

    def enqueue(self, item):
        """add item to top"""
        queue_item = (item, str(uuid.uuid4()))
        self.the_queue.append(queue_item)
        self.logger.debug('enqueue: %s' % queue_item[1])

        return 'ok', queue_item[1]

    def dequeue(self, item_uuid):
        """remove item from middle of queue"""
        result = 'error'
        message = ''
        for index, item in enumerate(self.the_queue):
            if item[1] == item_uuid:
                del self.the_queue[index]
                result = 'ok'
                break
        if result == 'ok':
            message = 'dequeue succeed: %s' % item_uuid
            self.logger.debug(message)
        else:
            message = 'dequeue failed: %s' % item_uuid
            result = 'error'
        self.logger.debug(message)

        return result, message

    def start_worker(self):
        """start a worker in a new thread"""
        result = 'ok'
        message = 'starting worker'

        if self.active == 'off':
            self.active = 'on'
            self.working_thread.start()
            self.logger.info('starting worker')
        else:
            result = 'error'
            message = 'cannot start worker, already exists'
            self.logger.error(message)

        return result, message

    def stop_worker(self):
        """stop worker"""
        result = 'ok'
        message = 'stopping worker'

        if self.active == 'on':
            self.active = 'stopping'
            self.logger.info('stopping worker')
        else:
            result = 'error'
            message = 'cannot stop worker, current status: %s' % self.active
            self.logger.error(message)

        return result, message

    def height(self):
        """return hight of queue"""
        return 'ok', len(self.the_queue)
