"""module unit tests
its kind of tricky to test server
for more info read the README.md
coverage """
import json
import time
from threaded_queue import AddingMachine
from logger import Logger
from input_process import InputProcessor

def adding_machine_test():
    """test: ENQ DEQ START STOP STATUS"""
    machine = AddingMachine(Logger())
    #check height
    assert machine.height()[0] == 'ok'
    assert machine.height()[1] == 0
    #check enqueue
    status, uuid = machine.enqueue('0')
    assert status == 'ok'
    assert len(uuid) == 36
    #check dequeue
    assert machine.dequeue(uuid)[0] == 'ok'
    #check worker starts
    assert machine.start_worker()[0] == 'ok'
    time.sleep(2)
    assert machine.height()[1] == 0
    #check worker stops
    assert machine.stop_worker()[0] == 'ok'
    machine.enqueue('0')
    assert machine.height()[1] == 1

def logger_test():
    """test: info, debug, error, write log"""
    assert Logger().info('unit test') is None
    assert Logger().debug('unit test') is None
    assert Logger().error('unit test') is None
    #check levels
    assert Logger().set_level('0')[0] == 'ok'
    assert Logger().set_level('1')[0] == 'ok'
    assert Logger().set_level('2')[0] == 'error'

def input_process_test():
    """test handler"""
    logger = Logger()
    machine = AddingMachine(logger)
    input_processor = InputProcessor(machine, logger)
    #check commands
    result = input_processor.process_message(str.encode(json.dumps({"command": "STATUS"})))
    assert json.loads(result)['status'] == 'error'
    result = input_processor.process_message(str.encode(json.dumps({"command": "STAT"})))
    assert json.loads(result)['status'] == 'ok'
    result = input_processor.process_message(str.encode(json.dumps({"command": "START"})))
    assert json.loads(result)['status'] == 'ok'
    result = input_processor.process_message(str.encode(json.dumps({"command": "STOP"})))
    assert json.loads(result)['status'] == 'ok'
    result = input_processor.process_message(str.encode(json.dumps({"command": "ENQ",
                                                                    "data": '1'})))
    assert result
    result_message = json.loads(result)['message']
    result = input_processor.process_message(str.encode(json.dumps({"command": "DEQ",
                                                                    "data": result_message})))
    assert json.loads(result)['status'] == 'ok'
    input_processor.process_message('')

adding_machine_test()
logger_test()
input_process_test()
