"""run this file to init server $ python3 main.py"""
from server import Server
from input_process import InputProcessor
from threaded_queue import AddingMachine
from logger import Logger

if __name__ == "__main__":
    print('init server')
    LOGGER_OBJECT = Logger()
    LOGGER_OBJECT.info('init server')
    INPUT_PROCESSOR = InputProcessor(AddingMachine(LOGGER_OBJECT), LOGGER_OBJECT)
    SERVER_OBJECT = Server(INPUT_PROCESSOR, LOGGER_OBJECT)
    SERVER_OBJECT.runserver()
    # when finished closing
    LOGGER_OBJECT.info('Closing!!')
    print('Closing!!')
