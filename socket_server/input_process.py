"""input handler,
processes data from server
routes data to other classes
compiles a reply
returns reply to server"""
import json


class Reply():
    """reply object"""

    def __init__(self):
        self.json_response = {}
        self.error = ''
        self.message = ''
        self.status = 'ok'

    def send(self):
        """compile reply strings into json for return to server"""
        self.json_response['error'] = self.error
        self.json_response['message'] = self.message
        self.json_response['status'] = self.status

        return json.dumps(self.json_response)


class InputProcessor():
    """process object
    process message
    send work to adding_machine
    send return data to reply
    send_out reply
    """
    def __init__(self, adding_machine, logger):
        self.adding_machine = adding_machine
        self.logger = logger

    def validate_message(self, message, reply):
        """validate inputs from client
        check data quality
        other tests can be added here"""
        try:
            message = json.loads(message)
            assert message['command'] is not None

        except Exception:
            error_message = "recieved an invalid message: %s" % message
            self.logger.error(error_message)
            reply.status = 'error'
            reply.error = error_message
            message = False
        return message

    def process_message(self, message):
        """compare command to list and execute
        if no match or invalid format then return an error"""
        reply = Reply()
        message = self.validate_message(message, reply)
        if message:
            result = 'ok'
            process_response = ''

            # run commands
            if message['command'] == 'ENQ':
                if 'data' in message:
                    result, process_response = self.adding_machine.enqueue(message['data'])
                else:
                    result, process_response = 'error', 'data required'
            elif message['command'] == 'DEQ':
                if 'data' in message:
                    result, process_response = self.adding_machine.dequeue(message['data'])
                else:
                    result, process_response = 'error', 'data required'

            elif message['command'] == 'STAT':
                result, process_response = self.adding_machine.height()
            elif message['command'] == 'START':
                result, process_response = self.adding_machine.start_worker()
            elif message['command'] == 'STOP':
                result, process_response = self.adding_machine.stop_worker()
            elif message['command'] == 'DEBUG':
                if 'data' in message:
                    result, process_response = self.logger.set_level(message['data'])
                else:
                    result, process_response = 'error', 'data required'

            else:
                result, process_response = 'error', 'unknown command %s' % message['command']
                self.logger.error(process_response)

            # compile reply
            if result == 'error':
                reply.status = result
                reply.error = process_response
            else:
                reply.message = process_response

        return reply.send()
