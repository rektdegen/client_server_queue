"""server"""
import sys
import socketserver
import traceback


class Server():
    "multithreaded server class can handle serveral connections at once"
    def __init__(self, processor, logger):
        self.host = "localhost"
        self.port = 9999
        self.processor = processor
        self.logger = logger

    class Handler(socketserver.BaseRequestHandler):
        """
        The request handler class for server.
        """

        def handle(self):
            # self.request is the TCP socket connected to the client
            try:
                self.data = self.request.recv(1024).strip()
                # process message
                # send work to adding_machine
                # send return data to reply
                # send_out reply
                # response = Processor().respond_to(self.request.recv(1024))
                response = str.encode(self.server.processor.process_message(self.data))
                self.request.sendall(response)
            except Exception:
                self.server.logger.error(traceback.format_exc())

    def runserver(self):
        """Create the server, binding to localhost on port 9999"""
        try:
            with socketserver.ThreadingTCPServer((self.host, self.port),
                                                 self.Handler) as server:
                server.logger = self.logger
                server.processor = self.processor
                # Activate the server
                server.serve_forever()

        except KeyboardInterrupt:
            print("You pressed Ctrl+C!")
            self.processor.logger.info('close server!')
            server.shutdown()
            sys.exit(0)
        except Exception:
            # log all other server exceptions
            print(traceback.format_exc())
            self.processor.logger.error(traceback.format_exc())
