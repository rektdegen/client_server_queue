"""logging module"""
import time


class Logger():
    """logger class
    3 log types
    enter and exit debug mode"""
    def __init__(self):
        self.debug_level = 1

    def write_log(self, message, file_name):
        """write a message to a file"""
        logging_string = '%s - %s \n' % (time.asctime(), message)
        with open(file_name, "a") as file:
            file.write(logging_string)

    def info(self, message):
        """write info log"""
        self.write_log(message, 'info.log')

    def debug(self, message):
        """always recieves debug messages, doesnt do anything is debug is off"""
        if self.debug_level:
            self.write_log(message, 'debug.log')

    def error(self, message):
        """write error, maybe also debug log"""
        self.write_log(message, 'error.log')
        if self.debug_level:
            self.write_log(message, 'debug.log')

    def set_level(self, level):
        """set debug level"""
        result = 'ok'
        message = ''

        if level == '0':
            message = 'exiting debug level'
            self.debug_level = 0
            self.debug(message)
        elif level == '1':
            self.debug_level = 1
            message = 'entering debug level'
            self.debug(message)
        else:
            result = 'error'
            message = 'unknown debug level %s' % level
            self.error(message)

        return result, message
