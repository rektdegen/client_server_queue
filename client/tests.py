"""
system tests

run these tests while server is online
if server is offline tests will fail!
"""

from time import sleep
import json
from main import Client

def queue_tests():
    "test enqueue"
    result = Client().send('STAT', None)
    assert json.loads(result)['message'] == 0
    result = Client().send('ENQ', 0)
    uuid = json.loads(result)['message']
    assert len(uuid) == 36  # is a uuid

    result = Client().send('DEQ', uuid)
    assert json.loads(result)['status'] == 'ok'
    assert json.loads(result)['message'] == "dequeue succeed: %s" % uuid

    result = Client().send('DEQ', uuid)
    assert json.loads(result)['status'] == 'error'
    assert json.loads(result)['error'] == "dequeue failed: %s" % uuid

    print('\n finished worker tests \n')


def worker_tests():
    "test worker"
    result = Client().send('START', None)
    assert json.loads(result)['message'] == 'starting worker'
    result = Client().send('START', None)
    assert json.loads(result)['error'] == 'cannot start worker, already exists'
    sleep(3)
    result = Client().send('STOP', None)
    assert json.loads(result)['message'] == 'stopping worker'
    result = Client().send('STOP', None)
    assert json.loads(result)['error'] == 'cannot stop worker, current status: stopping'
    sleep(1)
    result = Client().send('STOP', None)
    assert json.loads(result)['error'] == 'cannot stop worker, current status: off'
    # test that worker worked
    result = Client().send('STAT', None)
    assert json.loads(result)['message'] == 0

    print('\n finished worker tests \n')


def debug_tests():
    "test debug_level"
    result = Client().send('DEBUG', '0')
    assert json.loads(result)['message'] == "exiting debug level"

    result = Client().send('DEBUG', '1')
    assert json.loads(result)['message'] == "entering debug level"

    result = Client().send('DEBUG', '2')
    assert json.loads(result)['error'] == "unknown debug level 2"

    print('\n finished debug tests \n')


queue_tests()
worker_tests()
debug_tests()
