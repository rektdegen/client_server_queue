"""client module"""
import socket
import sys
import json


class Client():
    "client object, send commands via send function or sys.argv"
    def __init__(self):
        self.host = "localhost"
        self.port = 9999

    def send(self, command, data):
        """send command to server, return and print result"""
        json_message = {}
        json_message['command'] = command
        if data is not None:
            json_message['data'] = data

        print(json.dumps(json_message))

        # Create a socket (SOCK_STREAM means a TCP socket)
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            # Connect to server and send data
            sock.connect((self.host, self.port))
            sock.sendall(bytes(json.dumps(json_message), "utf-8"))

            # Receive data from the server and shut down
            received = str(sock.recv(1024), "utf-8")
        print(received)
        return received


if __name__ == "__main__":

    COMMAND = sys.argv[1]
    DATA = None
    if len(sys.argv) > 2:
        DATA = sys.argv[2]

    Client().send(COMMAND, DATA)
