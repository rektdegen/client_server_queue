# Queue Server and Client

The goal of this project is to review how you write code in Python

The project will simulate an implementation of Queue server that receives actions (enq, deq, debug, start, and stop) from multiple clients

* I'll check clean code, logic, abstraction, debug messages, unit tests

### General Comments

This project demonstrates knowledge of:
    
* multithreading
* sockets
* logging levels
* unit testing
* coverage
* system testing
* clean code (linting), pep8
* logic
* OOP concepts
* abstraction

# Prerequisites

Tested on Python 3.7.2

No requirements to run the client/server

I use coverage for testing

    $ sudo pip3 install coverage


# Run server

Clone this:

    $ git clone https://rektdegen@bitbucket.org/rektdegen/client_server_queue.git
 
Move to directory:

    $ cd client_server_queue 
    
Run server:

    $ python3 socket_server/main.py
    
# Tests
    
To run the server unit tests:
    
    $ coverage run socket_server/tests.py
    $ coverage report -m 
    
To run client/server system tests (server must be running):
    
    $ coverage run client/tests.py
    $ coverage report -m
    
# Client methods for interaction
    
    $ python3 client/main.py <command> <data>

### Queue Status:
    
    $ python3 client/main.py STAT
    
returns number of items in queue

### Enqueue:
    
    $ python3 client/main.py ENQ <string>
    
adds new item to queue

returns uuid reference number

### Dequeue:
    
    $ python3 client/main.py DEQ <uuid>
    
removes item from queue by reference id 

returns status 'ok' if success

returns status 'error' is no uuid found

### Start:
    
    $ python3 client/main.py START
    
starts queue worker in new thread

worker processes 1 item per second

returns status 'ok' if success

returns status 'error' if worker already exists 

### Stop:
    
    $ python3 client/main.py STOP
    
sends stop signal to queue worker

worker finishes task and then exits

returns status 'ok' if success

returns status 'error' if worker is not running 

### Debug:
    
    $ python3 client/main.py DEBUG <0|1>
    
toggles debug level to on or off

returns status 'ok' if success

returns status 'error' if value is not 0 or 1


# Authors

* **Rekt Degen** - [Blog and Research](https://rektdegen.wordpress.com)

# License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
